################################################################################
# Nome:     MAURÍCIO DE LIMA
# Projeto:  https://uaiso.org
# E-mail:   mauricio@uaiso.org
#
# Descrição: Utilitário para identificar o DE e fazer ajustes
#
# Uso: ./enable-de.sh
#
# Versão: 1.0
# Data: $(date +'%d/%m/%Y')
# Mantenedor: MAURÍCIO DE LIMA
################################################################################

pacote=budgie-desktop
if pacman -Qs $pacote > /dev/null ; then
    rm /usr/share/xsessions/gnome*
    rm /usr/share/wayland-sessions/gnome*
fi





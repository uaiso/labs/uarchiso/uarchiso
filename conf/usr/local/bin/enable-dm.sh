################################################################################
# Nome:     MAURÍCIO DE LIMA
# Projeto:  https://uaiso.org
# E-mail:   mauricio@uaiso.org
#
# Descrição: Utilitário para identificar o DM instalado e ativá-lo.
#
# Uso: ./enable-dm.sh
#
# Versão: 1.0
# Data: $(date +'%d/%m/%Y')
# Mantenedor: MAURÍCIO DE LIMA
################################################################################

pacote=sddm
if pacman -Qs $pacote > /dev/null ; then
    systemctl enable sddm.service
fi

pacote=gdm
if pacman -Qs $pacote > /dev/null ; then
    systemctl enable gdm.service
fi

pacote=lxdm
if pacman -Qs $pacote > /dev/null ; then
    systemctl enable lxdm.service
fi

pacote=lightdm
if pacman -Qs $pacote > /dev/null ; then
   systemctl enable lightdm.service
   pacman -S --need --noconfirm lightdm-slick-greeter lightdm-settings
fi

pacote=lightdm-slick-greeter
if pacman -Qs $pacote > /dev/null ; then
  sed -i '/greeter-session=/s/.*/greeter-session=lightdm-slick-greeter/' /etc/lightdm/lightdm.conf
fi

pacote=ly
if pacman -Qs $pacote > /dev/null ; then
  systemctl enable ly.service
fi

pacote=slim
if pacman -Qs $pacote > /dev/null ; then
  systemctl enable slim.service
fi



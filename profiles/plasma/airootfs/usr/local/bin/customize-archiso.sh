#!/bin/bash

# Executa o comando systemctl para habilitar o NetworkManager
systemctl enable NetworkManager
#systemctl enable systemd-resolved

pacman-key --populate archlinux chaotic uaiso

# Executa o comando locale-gen para gerar as configurações de localização
locale-gen

# Habilita o SDDM para o Plasma
systemctl enable sddm.service

# Habilita o Bluetooth
systemctl enable bluetooth
#

# Habilita Firealld
#systemctl enable firewalld.service
#

# Habilitar Cups
#systemctl enable cups
#
